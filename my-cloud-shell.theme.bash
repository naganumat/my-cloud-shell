# my cloud shell

function set_color() {
	local fg='' bg=''
	if [[ "${1:-}" != "-" ]]; then
		fg="38;5;${1}"
	fi
	if [[ "${2:-}" != "-" ]]; then
		bg="48;5;${2}"
		[[ -n "${fg}" ]] && bg=";${bg}"
	fi
	echo -e "\[\033[${fg}${bg}m\]"
}

function prompt() {
  local bar_color="${normal}$(set_color 33 -)"

  local project_color="$(set_color 39 237)"
  local project="${project_color} $(echo $DEVSHELL_PROJECT_ID) "

  local username_color="$(set_color 105 236)"
  local username="${username_color} $(echo $USER_EMAIL) "

  local scm_color="$(set_color 209 235)"
  local scm=$(scm_prompt_info)
  [[ ! -z "$scm" ]] && scm="${scm_color}${scm} "

  local workdir_color="${normal}$(set_color 246 -)"
  local workdir="${workdir_color} \w "
 
  PS1="\n${bar_color}┌─${project}${username}${scm}${normal}\n${bar_color}│ ${workdir}${normal}\n${bar_color}└▪${normal} "
}

safe_append_prompt_command prompt
