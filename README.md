# My Cloud Shell

A [bash-it] theme for [Google Cloud Shell].

[bash-it]: https://github.com/Bash-it/bash-it
[Google Cloud Shell]: https://cloud.google.com/shell

![Screen shot](screenshot.png)

## Install

1. Install [bash-it].
2. Clone the repository to bash-it themes directory.

    ```sh
    git clone https://gitlab.com/naganumat/my-cloud-shell \
        ~/.bash_it/themes/my-cloud-shell
    ```
3. Set my-cloud-shell to bash-it theme in ~/.bashrc.

    ```bash
    export BASH_IT_THEME='my-cloud-shell'
    ```

